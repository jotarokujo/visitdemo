﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourScript : MonoBehaviour
{
    public enum RotationAxes
    {
        MouseXAndY = 0,
        MouseX = 1,
        MouseY = 2
    }
    
    public RotationAxes m_axes = RotationAxes.MouseXAndY;
    public float minX = -360;
    public float maxX = 360;
    public float minY = 0;
    public float maxY = 45;
    float m_rotationY = 0;

    public float movespeed = 50;
    public float rotatepeed = 50;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0)) 
        {
            switch (m_axes) 
            {
                case (RotationAxes.MouseXAndY):
                    float m_rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * rotatepeed * Time.deltaTime;
                    m_rotationY += Input.GetAxis("Mouse Y") * rotatepeed * Time.deltaTime;
                    m_rotationY = Mathf.Clamp(m_rotationY, minY, maxY);
                    transform.localEulerAngles = new Vector3(-m_rotationY, m_rotationX, 0);
                    break;
                case (RotationAxes.MouseX):
                    transform.Rotate(0, Input.GetAxis("Mouse X") * rotatepeed * Time.deltaTime, 0);
                    break;
                case (RotationAxes.MouseY):
                    m_rotationY += Input.GetAxis("Mouse Y") * rotatepeed * Time.deltaTime;
                    m_rotationY = Mathf.Clamp(m_rotationY, minY, maxY);

                    transform.localEulerAngles = new Vector3(-m_rotationY, transform.localEulerAngles.y, 0);
                    break;
            }
        }

        if (Input.GetMouseButton(1)) 
        {
            float x = Input.GetAxis("Mouse X") * movespeed * Time.deltaTime;
            float z = Input.GetAxis("Mouse Y") * movespeed * Time.deltaTime;
            this.transform.Translate(x,0,z,Space.World);
        }
    }

                    
            }
                
        
        
        
        
        
    

